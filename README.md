  <p>A geographic bounding box for each country from Natural Earth’s <a href="http://www.naturalearthdata.com/downloads/110m-cultural-vectors/110m-admin-0-countries/">1:110m Cultural Vectors (Admin 0 - Countries)</a>.

 <p>A <a href="http://en.wikipedia.org/wiki/Minimum_bounding_box">minimum bounding box</a> in <i>geographic coordinates</i> is an area defined by minimum and maximum longitudes and latitudes. 

<p># <b>d3.geo.bounds</b>(<i>feature</i>)  -- (<a href="https://github.com/mbostock/d3/wiki/Geo-Paths#wiki-bounds">source</a>)</p>

<p>Returns the spherical bounding box for the specified feature. The bounding box is represented by a two-dimensional array: <i>[​[left, bottom], [right, top]​]</i>, where left is the minimum longitude, bottom is the minimum latitude, right is maximum longitude, and top is the maximum latitude.</p>

<p>
<b>Data:</b> see console of full windows view, with data such :<br /> 
<code>{"W":97.3717371737174; "S":5.688035433543348; "E":105.58055805580562; "N":20.414615115511538; "item":"Thailand"},</code>
</p>


## Bugs

* centroid symbols seem to come from the opposit hemishpere too

forked from <a href='http://bl.ocks.org/hugolpz/'>hugolpz</a>'s block: <a href='http://bl.ocks.org/hugolpz/6391065'>Geographic Bounding Boxes</a>